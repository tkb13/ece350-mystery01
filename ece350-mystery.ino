// ECE 350 "MYSTERY01" chip firmware
// for the ATTINY84 microcontroller
//
// 2023-09-15 - v1 - Tyler Bletsch - initial release
//

//#include <SoftwareSerial.h> // can't use software serial because it block pin change interrupts for receive. a homemade set of serial transmit functions are included for debug output.


// pin assignments
const int PIN_ANALOG_IN = A0; // for reading and validating the expected 3.3V input
const int PIN_MSG[] = {7,8,9,10}; // pins for the 4bit bus emitting the message data out, LSB first, MSB last
const int PIN_MSG_COUNT = sizeof(PIN_MSG)/sizeof(*PIN_MSG); // number of pins in the bus
const int PIN_MCLK = A3; // pin for the message data out clock

// settings for crapSerialSend
const int PIN_TXD = A2;  // pin number in arduino land
#define TXD_PORT PORTA // port register for the same pin
#define TXD_PIN PA2 // pin number within the port for the same pin
#define BAUD_RATE 9600

// settings for pin change interrupt -- see ATTINY84 datasheet!
const int PIN_WAVE_IN = A1; // for reading and validating the 10kHz square wave input
#define PIN_WAVE_IN_PCIE  PCIE0  // the pin change interrupt enable bit location to be set in GIMSK (see datasheet), corresponds to pin chosen by PIN_WAVE_IN
#define PIN_WAVE_IN_PCMSK PCMSK0 // the pin change interrupt mask register to use (see datasheet), corresponds to pin chosen by PIN_WAVE_IN
#define PIN_WAVE_IN_PCINT PCINT1 // the pin selector bit location for PIN_WAVE_IN_PCMSK (see datasheet or an ATTINY84 pinout image), corresponds to pin chosen by PIN_WAVE_IN


// want this voltage +/- the tolerance
const int ANALOG_TARGET = 3.3/5.0*1024; // 3.3V
const int ANALOG_TOL = 0.33/5.0*1024; // +/- 0.33 V (10%)

// want this wave frequency +/- the tolerance
const int WAVE_TARGET_HZ = 10000; // 10kHz
const int WAVE_TOL_HZ = 1000; // +/- 1kHz (10%) -- note: the internal oscillator has a fair bit of innaccuracy, but 10% should still be plenty of margin)

const int MESSAGE[] = {0xe, 0xc, 0xe, 0x3, 0x5, 0x0}; // the secret message to shift out 4 bits at a time: 0xECE350
const int MESSAGE_LEN = sizeof(MESSAGE)/sizeof(*MESSAGE); // number of elements

// incremented by pin change interrupt to count edges on 
volatile int edges=0;


void setup() {
  pinMode(PIN_ANALOG_IN, INPUT);
  pinMode(PIN_WAVE_IN, INPUT);
  for (int i=0; i<PIN_MSG_COUNT; i++) {
    pinMode(PIN_MSG[i], OUTPUT);
  }
  pinMode(PIN_MCLK, OUTPUT);

  // Enable Pin Change Interrupt for the PCIE group corresponding to PIN_WAVE_IN
  GIMSK |= (1 << PIN_WAVE_IN_PCIE);

  // Enable the PCINT mask for just the pin corresponding to PIN_WAVE_IN
  PIN_WAVE_IN_PCMSK = (1 << PIN_WAVE_IN_PCINT);

  // Enable global interrupts
  sei();

  pinMode(PIN_TXD,OUTPUT);
  crapSerialSend("Booted\n");
}

// return true if the v is between target-tol and target+tol
bool within(int v, int target, int tol) {
  return (v>=(target-tol)) && (v<=(target+tol));
}

void loop() {
  long t_start = micros(); // get timestamp for now and zero out the automatically-increasing edges variable
  edges = 0; 
  delay(100); // edges is being incremented by the pin change interrupt; we're counting how many during this delay
  long edges_now = edges; // this is the count of edges seen this recent delay
  long t_delta = micros() - t_start; // measure the time rather than just assume 100ms
  long us_per_wave = t_delta / (edges_now / 2); // edges/2 because that's both rising and falling and we want wavelength
  long freq_hz = 1000000/us_per_wave; // 1000000 microseconds-per-second / microseconds-per-wave gives waves per second, or Hz
  bool wave_ok = within(freq_hz, WAVE_TARGET_HZ, WAVE_TOL_HZ); // is the wave the right speed?

  int ain_value = analogRead(PIN_ANALOG_IN); // measure incoming voltage
  bool ain_ok = within(ain_value, ANALOG_TARGET, ANALOG_TOL); // is the voltage close enough?

  bool overall_ok = wave_ok && ain_ok; // if both checks good, we're clear to emit secret

  // use homemade serial prints to report status
  crapSerialSend("ain_value: ");
  crapSerialSend(ain_value);
  crapSerialSend(" ain_ok: ");
  crapSerialSend(ain_ok);
  crapSerialSend(" edges_now: ");
  crapSerialSend(edges_now);
  crapSerialSend(" us_per_wave: ");
  crapSerialSend(us_per_wave);
  crapSerialSend(" t_delta: ");
  crapSerialSend(t_delta);
  crapSerialSend(" freq_hz: ");
  crapSerialSend(freq_hz);
  crapSerialSend(" wave_ok: ");
  crapSerialSend(wave_ok);
  crapSerialSend("\n");
  
  // shift out the message 4 bits at a time on success
  if (overall_ok) { 
    crapSerialSend("Emitting secret!\n");  
    for (int i=0; i<MESSAGE_LEN; i++) {
      for (int b=0; b<PIN_MSG_COUNT; b++) {
        digitalWrite(PIN_MSG[b], MESSAGE[i]&(1<<b));
      }
      digitalWrite(PIN_MCLK, 1); // clock high for data ready
      delay(800);
      digitalWrite(PIN_MCLK, 0); // clock low between 4bit words
      delay(200);
    }
    // data outputs and clock go low between transmissions
    for (int b=0; b<PIN_MSG_COUNT; b++) {
      digitalWrite(PIN_MSG[b], 0);
    }    
    delay(1000); // clock low for a full second between transmissions
  } else {
    // inputs bad, just keep outputs low
    for (int b=0; b<PIN_MSG_COUNT; b++) {
      digitalWrite(PIN_MSG[b], 0);
    }    
    digitalWrite(PIN_MCLK, 0);
  }
  
}

// Software-based serial transmission
void crapSerialSend(char data) {
  cli(); // interrupts screw up our timing, suspend them during transmission

  // we use low-level port-and-pin bit flipping rather than digitalWrite for speed
  
  // Send start bit (low)
  TXD_PORT &= ~(1 << TXD_PIN); // Set TXD_PIN low
  delayMicroseconds(1000000 / BAUD_RATE); // Delay for one bit period
  for (int i = 0; i < 8; i++) {
    if (data & 1) {
      TXD_PORT |= (1 << TXD_PIN); // Set TXD_PIN high
    } else {
      TXD_PORT &= ~(1 << TXD_PIN); // Set TXD_PIN low
    }
    data >>= 1;
    delayMicroseconds(1000000 / BAUD_RATE); // Delay for one bit period
  }

  // Send stop bit (high)
  TXD_PORT |= (1 << TXD_PIN);
  delayMicroseconds(1000000 / BAUD_RATE); // Delay for one bit period

  sei(); // re-enable interrupts
}

// send a string
void crapSerialSend(char* buf) {
  while (*buf) {
    crapSerialSend(*buf);
    buf++;
  }
}

// send an int
void crapSerialSend(int v) {
  char buf[10];
  itoa(v,buf,10);
  crapSerialSend(buf);
}

// send a long
void crapSerialSend(long v) {
  char buf[20] = {0};
  ltoa(v,buf,10);
  crapSerialSend(buf);
}

// Pin Change Interrupt Service Routine (ISR)
// called when the PIN_WAVE_IN toggles
ISR(PCINT0_vect) {
  edges++; // increment the edge count (a global variable read elsewhere)
}
