# ECE 350 "MYSTERY01" chip firmware

To allow students to practice using bench instruments (power supply, multimeter, wave generator, and oscilloscope), this firmware, when applied to an ATTINY84 microcontroller, creates a challenge involving every piece of gear at once.

See the lab write-up for details, but the short of it is that students must:
- Power the ATTINY84 with GND (physical pin 14) and 5V on VCC (physical pin 14)
- Also supply arduino pin A0 (physical pin 13) with 3.3V
- Supply a 0V..5V 10kHz square wave on arduino pin A1 (physical pin 12)

Upon success, the chip will shift out a message on a 4bit data bus (arduino pins 10,9,8,7; physical pins 2, 3, 5, 6) with a data clock emitted on arduino pin A3 (physical pin 10). Each 4-bit word will be sent with a clock period of one second, with one second of delay between repetitions of the message. 

Additionally, debug serial prints are provided on arduino pin A2 (physical pin 11).

In the fiction of the lab write-up, the chip provided is a "Duke MYSTERY01", but it's really an Atmel ATTINY84 with this firmware.

## Flashing/debugging

The firmware can be flashed using the MISO/MOSI/SCK pins of the ATTINY84 using any Arduino-compatible flashing device, including a another repurposed Arduino (see [Arduino as ISP](https://docs.arduino.cc/built-in-examples/arduino-isp/ArduinoISP)). I used a cheap and commonly available [USBasp](https://www.amazon.com/Geekstory-Microcontroller-Programmer-Downloader-Adapter/dp/B07NZ59VK2). You'll need to make a breakout cable, probably out of male/female dupont wires. If you want to follow the same color standard that I do, here's my dumb mnemonic:
- VCC = Red (duh)
- GND = Black (duh)
- RESET = White (blank slate)
- SCK = Yellow (old socks turn yellow)
- MISO = Green (miso soup is kinda green)
- MOSI = Blue (the other color left after you follow the above)


To prepare Arduino IDE, install the ATTINY support from [here](https://github.com/damellis/attiny).

To flash a new chip, hook your programmer to GND, MISO, MOSI, SCK, and RESET. Hook VCC if you want the programmer to be the thing that powers the chip. First, do "Tools | Burn bootloader"; this just sets the fuses to the chip uses an internal clock of 8MHz. Then just upload the firmware normally.

To see debug output, you need a separate USB serial transceiver (e.g. [this one](https://www.amazon.com/IZOKEE-CP2102-Converter-Adapter-Downloader/dp/B07D6LLX19)). Hook GND up, then transceiver RX to ATTINY84's A2 (physical pin 11). Set the COM port in Arduino to the one provided by the transceiver, then you can open the serial monitor at 9600 baud. 

## Labeling

To make new MYSTERY01 chips like the ones I did, you can use a Brother-compatible label maker with 1/4" (6mm) tape. The text is:
  ```
  MYSTERY01
  Duke [MM]/[YY] v[VERSION]
  ```
Where [MM] is the two-digit month, [YY] is the two-digit year, and [VERSION] is the version number of the firmware. So the initial run was labeled:
  ```
  MYSTERY01
  Duke 09/23 v1
  ```

## Testing

To test, hook inputs as described above. Hook a logic analyzer or scope to the 4-bit message output bus plus the message output clock. If your analyzer can interpret the 4-bit bus as hex, bonus.

Then, either watch it say out "E", "C", "E", "3", "5", "0" in time with the clock, or switch your scope to slow rolling mode and watch it draw the waves out. 

No scope? You can at least slap LEDs on the serial output and message clock pins. When the chip is failing to get good inputs, you'll get no clock but twinkling pulsing on the serial pin (the debug prints). When the chip is emitting the message, you'll get 0.8 second pulses on the clock and steady-on on the serial pin (since it's not printing during messaging).

## License

Public domain. Go hog wild.
